const express = require('express');
const Router = express.Router();
const controllers = require('./controllers');

Router
    .get('/',controllers.index)
    .post('/chat',controllers.chat)

module.exports = Router;